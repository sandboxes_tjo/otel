docker login -u tjo-token -p $TOKEN  allmytalks.azurecr.io

```bash
dest_cr=allmytalks.azurecr.io
for img in $(grep 'IMG' .env |tr -d '\r'| cut -d'=' -f2); do
    echo move ${img} to ${dest_cr}
    docker image tag ${img} ${dest_cr}/${img}
    docker image push ${dest_cr}/${img}
done
```