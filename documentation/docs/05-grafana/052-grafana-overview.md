---
sidebar_position: 0
sidebar_label: Grafana Overview
title: Grafana Overview
---

![ ](img/grafana_overview.png)

The menus that will interest us:

- Dashboards : this is where we will find all the dashboards
- Explore : this is where we can directly query the datasources

For information there is a library of dashboards created by the community : [Community dashboard](https://grafana.com/grafana/dashboards/)