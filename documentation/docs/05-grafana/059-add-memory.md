---
sidebar_position: 0
sidebar_label: Add Memory metrics
title: Add Memory metrics
---

## Memory Part

You'll need to enter the following information

- In the top right-hand corner, select *Logs**
- Choose **mimir** as the data source
- Switch to **Code mode** for the query

```txt
sum by(job) (process_runtime_jvm_memory_usage_bytes)
```

- You can change the widget title

![ ](img/widget-memory.png)

Then click on save next to apply.
