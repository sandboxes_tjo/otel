---
sidebar_position: 0
sidebar_label: Add stock report
title: Add stock report
---

For the first widget, we're going to add a stock report directly from the postgres database.

You'll need to enter the following information

- In the top right-hand corner, select **Bar chart**
- Choose **PostgreSQL** as the data source
- Switch to **Code mode** for the query

```sql
SELECT type, SUM(quantity) 
FROM stock 
GROUP BY type 
LIMIT 50 
```

- You can change the widget title

![ ](img/widget_postgres.png)

Then click on save next to apply, you'll be able to choose the name of the dashboard and the folder in which you want to save it

![ ](img/widget_save.png)
