---
sidebar_position: 0
sidebar_label: Build your first dashboard
title: Build your first dashboard
---

## Build a dashboard

In this exercise we will build a dashboard that displays the JVM metrics, logs and allows to request traces.

On the **Dashboard** screen, click on **New** then **New Dashboard**

To add a visualization, click on **add** and **Visualization**

![ ](img/grafana_add_visualization.png)
