---
sidebar_position: 0
sidebar_label: Add filters
title: Add filters
---

Grafana lets you add filters to dasbhoards using variables. We can define a variable that points to a metric, for example, and retrieve labels to feed our filter. Then we can make our widget filter with this value

First of all, in your dashboard settings, you need to

![ ](img/dashboard-param.png)

Then add a variable

![ ](img/param_add_vars.png)

The following fields must be completed

| Fields               | Value        |
| -------------------- | ------------ |
| Select variable type | Query        |
| Name                 | service      |
| Label                | Service      |
| Data source          | mimir        |
| Query type           | Label values |
| Label                | job |

![ ](img/add_vars.png)

At the bottom of the screen you can click on apply. Once back on the dashboard, you'll see your setting at the top of the screen.

![ ](img/dashboards-param-view.png)

We can now modify the various widgets to take the filter into account

For CPU widget

```txt
process_runtime_jvm_cpu_utilization_ratio{job="$service"}
```

For memory widget

```txt
sum by(job) (process_runtime_jvm_memory_usage_bytes{job="$service"})
```

For log widget
```txt
{exporter="OTLP", application="$service"} | json | line_format "{{ printf \"%-20.20s\" .application}}\t{{.level}}\t{{.body}}"
```

Now you can play with the parameter to display the desired micro service