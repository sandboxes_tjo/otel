---
sidebar_position: 0
sidebar_label: 🔥 Prerequisites
title: Prerequisites
---

:::caution
- Have a WSL with docker and docker-compose
- I also advise you to have visual studio code (or preffered IDE) connected to your WSL
:::caution
