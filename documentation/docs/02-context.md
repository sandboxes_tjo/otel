---
sidebar_position: 0
sidebar_label: Project Presentation
title: Project Presentation
---

## Overview

Welcome to Woodland company !!

Here is our efficient Information System. You have at your disposal our supply chain management system.

![ ](img/archi-overview.png)

## Components of the supply chain management

- Wood Store Dashboard: A frontend application (Angular based) displaying stocks evolution
- Furniture Store : Several client ordering deferent type of wood to the Lumber Camp
- Lumber Camp : Service managing the Wood production warehouse
- Postgres : The database

## Components of the observability stack

- Otel Collector : The api allowing us to send our metrics, logs and traces
- Mimir : The database for the metrics
- Loki : The database for the logs
- Tempo : The database for the traces
- Grafana : The visualization tool to see metrics, logs and traces

## How to run

The application is already packaged to run locally via a docker-compose file.

```bash
docker compose up -d
```

The WoodStore Dashboard is accessible here: [http://localhost:4200](http://localhost:4200)