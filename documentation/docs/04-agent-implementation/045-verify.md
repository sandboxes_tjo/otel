---
sidebar_position: 0
sidebar_label: Verify
title: Verify
---

## Check metrics

[Metrics on grafana](http://localhost:3000/explore?orgId=1&left=%7B%22datasource%22:%22mymimir%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22datasource%22:%7B%22type%22:%22prometheus%22,%22uid%22:%22mymimir%22%7D%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D)

You can use the following query `process_runtime_jvm_memory_usage_bytes{pool="G1 Old Gen"}` and check that you get one row per service.

![ ](img/prometheus_view.png)

## Check logs

[Logs on grafana](http://localhost:3000/explore?orgId=1&left=%7B%22datasource%22:%22myloki%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22%7Bapplication%3D%5C%22lumbercamp%5C%22%7D%20%7C%3D%20%60%60%22,%22queryType%22:%22range%22,%22datasource%22:%7B%22type%22:%22loki%22,%22uid%22:%22myloki%22%7D,%22editorMode%22:%22builder%22%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D)

![ ](img/loki_view.png)

## Check traces

[Traces on grafana](http://localhost:3000/explore?orgId=1&left=%7B%22datasource%22:%22mytempo%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22datasource%22:%7B%22type%22:%22tempo%22,%22uid%22:%22mytempo%22%7D,%22queryType%22:%22nativeSearch%22,%22limit%22:20,%22serviceName%22:%22lumbercamp%22%7D%5D,%22range%22:%7B%22from%22:%22now-1h%22,%22to%22:%22now%22%7D%7D)

![ ](img/tempo_view.png)