---
sidebar_position: 0
sidebar_label: Objectives
title: Objectives
---

:::info
Official documentation for automatic instrumentation : <https://opentelemetry.io/docs/instrumentation/java/automatic/>
:::

In this section you will add an open telemetry agent for Java on the differents backends.

This agent automatically retrieves metrics, traces and logs from our applications. It is configured directly in the JVM. No need to instrument code.

This allows us to quickly provide "standard" monitoring of our applications.
