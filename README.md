# observability-workshop

Welcome to this workshop about Observability.

## Prerequisites

In order to start the workshop you need to have installed *Docker* and *Docker compose*

## Start the Workshop

All component are build locally when you start the docker-compose.yml in the root directory

In order to build & start  all container use : 

```bash
docker compose -f docker-compose.yml  up -d 
```

To force rebuild all images : 

```bash
docker compose -f docker-compose.yml up --build -d
```

Only one service :
* lumbercamp
* furniturestore-[1-3]
* woodstoredashboard
* docusaurus

```bash
docker compose up -d --no-deps --build <service_name> 
```

Once every thing is build you can go to (Observability workshop)[https://yellow-pebble-0a42f3c03.3.azurestaticapps.net]
